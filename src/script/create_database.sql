DROP DATABASE IF EXISTS timezone;

CREATE DATABASE timezone;

USE timezone;

CREATE TABLE offset (
  id    INT         NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name  VARCHAR(10) NOT NULL,
  value INT         NOT NULL
);

INSERT INTO offset (name, value) VALUES
  ('GMT+0', 0), ('GMT+1', 1), ('GMT+2', 2), ('GMT+3', 3), ('GMT+4', 4), ('GMT+5', 5), ('GMT+6', 6), ('GMT+7', 7),
  ('GMT+8', 8), ('GMT+9', 9), ('GMT+10', 10), ('GMT+11', 11), ('GMT+12', 12), ('GMT+13', 13), ('GMT+14', 14),
  ('GMT-1', -1), ('GMT-2', -2), ('GMT-3', -3), ('GMT-4', -4), ('GMT-5', -5), ('GMT-6', -6), ('GMT-7', -7),
  ('GMT-8', -8), ('GMT-9', -9), ('GMT-10', -10), ('GMT-11', -11), ('GMT-12', -12);

CREATE TABLE user (
  id       INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  login    VARCHAR(20)  NOT NULL,
  password VARCHAR(30)  NOT NULL,
  name     VARCHAR(100) NOT NULL,
  enabled  BOOLEAN      NOT NULL,
  UNIQUE INDEX (login),
  INDEX (enabled)
);

CREATE TABLE user_role (
  user_id INT         NOT NULL,
  role    VARCHAR(15) NOT NULL,
  PRIMARY KEY (user_id, role(7)),
  FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE timezone (
  id        INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  user_id   INT          NOT NULL,
  name      VARCHAR(100) NOT NULL,
  city      VARCHAR(100) NOT NULL,
  offset_id INT          NOT NULL,
  FOREIGN KEY (offset_id) REFERENCES offset (id),
  FOREIGN KEY (user_id) REFERENCES user (id)
);

# INSERT INTO user (login, password, name, enabled) VALUES ('a', 'a', 'a', TRUE);
# INSERT INTO user_role VALUES (1, 'ROLE_ADMIN');
# INSERT INTO user_role VALUES (1, 'ROLE_MODERATOR');
