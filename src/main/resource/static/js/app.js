var TOKEN_KEY = "jwtToken";

angular.module('timezoneApp', ['ngRoute', 'ngResource', 'timezoneApp.services'])
    .config(function ($routeProvider, $httpProvider, $provide) {
        $routeProvider
            .when('/', {templateUrl: 'home.html', controller: 'home', controllerAs: 'controller'})
            .when('/login', {templateUrl: 'login.html', controller: 'navigation', controllerAs: 'controller'})
            .when('/register', {templateUrl: 'register.html', controller: 'register', controllerAs: 'controller'})
            .when('/timezone', {templateUrl: 'timezone.html', controller: 'timezoneView', controllerAs: 'controller'})
            .when('/timezoneAdd', {
                templateUrl: 'timezone_add.html',
                controller: 'timezoneAdd',
                controllerAs: 'controller'
            })
            .when('/timezoneEdit/:id', {
                templateUrl: 'timezone_edit.html',
                controller: 'timezoneEdit',
                controllerAs: 'controller'
            })
            .when('/moderator/timezone', {
                templateUrl: '/moderator/timezone.html',
                controller: 'moderatorTimezoneView',
                controllerAs: 'controller'
            })
            .when('/moderator/timezoneEdit/:id', {
                templateUrl: '/moderator/timezone_edit.html',
                controller: 'moderatorTimezoneEdit',
                controllerAs: 'controller'
            })
            .when('/admin/users', {
                templateUrl: '/admin/user.html',
                controller: 'adminUsersView',
                controllerAs: 'controller'
            })
            .when('/admin/userEdit/:id', {
                templateUrl: '/admin/user_edit.html',
                controller: 'adminUserEdit',
                controllerAs: 'controller'
            })
            .when('/api', {
                templateUrl: '/api.html',
                controller: 'api',
                controllerAs: 'controller'
            })
            .otherwise('/');

        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
        $httpProvider.defaults.headers.common['Authorization'] = localStorage.getItem(TOKEN_KEY);

        $provide.factory('errorInterceptor', function($q, ErrorHandler) {
            return {
                'response': function(response) {
                    ErrorHandler.handleOk(response);
                    return response;
                },

                'responseError': function(rejection) {
                    ErrorHandler.handleError(rejection);
                    return $q.reject(rejection);
                }
            };
        });
        $httpProvider.interceptors.push('errorInterceptor');
    })
    .controller('home', function ($http, $scope, $rootScope, $location) {})
    .controller('api', function ($http, $scope, $rootScope, $location) {})
    .controller('navigation', function ($rootScope, $http, $location, TokenHandler, AuthService) {
        var self = this;

        if (!$rootScope.offsets) {
            $http.get('metadata').then(function (response) {
                $rootScope.offsets = response.data.availableOffsets;
            });
        }

        self.credentials = {};
        if (!$rootScope.authenticated && TokenHandler.hasAuthToken()) {
            AuthService.authorize(function() {
                $location.path("/");
            });
        }

        self.login = function () {
            if (!self.credentials) {
                return;
            }
            AuthService.authenticate(self.credentials, function () {
                if ($rootScope.authenticated) {
                    $location.path("/");
                    self.error = false;
                } else {
                    $location.path("/login");
                    self.error = true;
                }
            });
        };

        $rootScope.logout = function() {
            AuthService.logout();
            $location.path("/");
        };

        $rootScope.closeError = function() {
            $rootScope.hasError = false;
        };
    })
    .controller('logout', function ($rootScope, $http, $location, TokenHandler, AuthService) {
        this.logout = function() {
            AuthService.logout();
            $location.path("/login");
        };
    })
    .controller('register', function ($rootScope, $location, $http) {
        var self = this;
        self.error = false;
        self.credentials = {};

        this.register = function () {
            $http.get('registerUser', {params: self.credentials}).then(function (response) {
                if (response.data.name) {
                    $location.path("/login");
                } else {
                    self.error = true;
                }
            }, function () {
                $rootScope.authenticated = false;
            });
        };
    })
    .controller('timezoneView', function ($http, $scope, $rootScope, $location, TimezoneDataFactory, TimezoneFactory, CurrentTimeFactory, AuthService) {
        AuthService.checkAuth();

        $scope.editTimezone = function (id) {
            $location.path("/timezoneEdit/" + id);
        };
        $scope.deleteTimezone = function (id) {
            TimezoneFactory.delete({id: id}).$promise.then(function() {
                $scope.timezoneData = TimezoneDataFactory.query();
            });
        };
        $scope.addTimezone = function () {
            $location.path("/timezoneAdd/");
        };
        $scope.timeInTimezone = function (offset) {
            var date = new Date($scope.currentTime.time + offset * 60 * 60 * 1000);

            var utcHours = date.getUTCHours();
            var utcMinutes = date.getUTCMinutes();
            return (utcHours < 10 ? "0" : "") +utcHours + ":" + (utcMinutes < 10 ? "0" : "") + utcMinutes;
        };
        $scope.filter = function () {
            $scope.timezoneData = TimezoneDataFactory.query({namePrefix: $scope.namePrefix});
        };

        $scope.currentTime = CurrentTimeFactory.get();
        $scope.timezoneData = TimezoneDataFactory.query();
    })
    .controller('timezoneAdd', function ($http, $scope, $rootScope, $window, $location, TimezoneDataFactory, AuthService) {
        AuthService.checkAuth();

        $scope.addTimezone = function () {
            TimezoneDataFactory.create($scope.timezone).$promise.then(function () {
                $location.path("/timezone");
            });
        };

        $scope.cancel = function () {
            $location.path("/timezone");
        };
    })
    .controller('timezoneEdit', function ($scope, $location, $routeParams, AuthService, TimezoneDataFactory, TimezoneFactory) {
        AuthService.checkAuth();

        $scope.updateTimezone = function (id) {
            var timezone = {
                id: id,
                city: $scope.timezone.city,
                offsetId: $scope.timezone.offsetId,
                name: $scope.timezone.name
            };
            if (!timezone.offsetId) {
                timezone.offsetId = $scope.timezone.timezoneOffset.id;
            }
            TimezoneFactory.update(timezone).$promise.then(function () {
                $location.path("/timezone");
            });
        };

        $scope.cancel = function () {
            $location.path("/timezone");
        };

        $scope.timezone = TimezoneFactory.show({id: $routeParams.id});
    })
    .controller('moderatorTimezoneView', function ($http, $scope, $rootScope, $location, ModeratorTimezoneFactory, CurrentTimeFactory, AuthService) {
        AuthService.checkModeratorRole();

        $scope.editTimezone = function (id) {
            $location.path("/moderator/timezoneEdit/" + id);
        };

        $scope.deleteTimezone = function (id) {
            ModeratorTimezoneFactory.delete({id: id}).$promise.then(function() {
                $scope.timezoneData = ModeratorTimezoneFactory.query();
            });
        };

        $scope.timeInTimezone = function (offset) {
            var date = new Date($scope.currentTime.time + offset * 60 * 60 * 1000);

            var utcHours = date.getUTCHours();
            var utcMinutes = date.getUTCMinutes();
            return (utcHours < 10 ? "0" : "") +utcHours + ":" + (utcMinutes < 10 ? "0" : "") + utcMinutes;
        };
        $scope.filter = function () {
            $scope.timezoneData = ModeratorTimezoneFactory.query({namePrefix: $scope.namePrefix});
        };

        $scope.currentTime = CurrentTimeFactory.get();
        $scope.timezoneData = ModeratorTimezoneFactory.query();
    })
    .controller('moderatorTimezoneEdit', function ($http, $scope, $rootScope, $location, $routeParams, ModeratorTimezoneFactory, AuthService) {
        AuthService.checkModeratorRole();

        $scope.updateTimezone = function (id) {
            var timezone = {
                id: id,
                city: $scope.timezone.city,
                offsetId: $scope.timezone.offsetId,
                name: $scope.timezone.name
            };
            if (!timezone.offsetId) {
                timezone.offsetId = $scope.timezone.timezoneOffset.id;
            }
            ModeratorTimezoneFactory.update(timezone).$promise.then(function () {
                $location.path("/moderator/timezone");
            });
        };

        $scope.cancel = function () {
            $location.path("/moderator/timezone");
        };

        $scope.timezone = ModeratorTimezoneFactory.show({id: $routeParams.id});
    })
    .controller('adminUsersView', function ($http, $scope, $rootScope, $location, AdminUserFactory, AuthService) {
        AuthService.checkAdminRole();

        $scope.editUser = function (id) {
            $location.path("/admin/userEdit/" + id);
        };

        $scope.deleteUser = function (id) {
            AdminUserFactory.delete({id: id}).$promise.then(function() {
                $scope.userData = AdminUserFactory.query();
            });
        };

        $scope.userData = AdminUserFactory.query();
    })
    .controller('adminUserEdit', function ($http, $scope, $rootScope, $location, $routeParams, AdminUserFactory, RolesFactory, AuthService) {
        AuthService.checkAdminRole();

        $scope.updateUser = function (id) {
            var user = $scope.user;
            user.id = id;
            AdminUserFactory.update(user).$promise.then(function () {
                $location.path("/admin/users");
            });
        };

        $scope.cancel = function () {
            $location.path("/admin/users");
        };

        $scope.userHasRole = function (role, user) {
            if (!user.resolved) {
                return false;
            }
            var userRoles = user.roles;
            for (var i = 0; i < userRoles.length; i++) {
                if (role == userRoles[i]) {
                    return true;
                }
            }

            return false;
        };

        $scope.availableRoles = RolesFactory.get();
        $scope.user = AdminUserFactory.show({id: $routeParams.id});
    });

var services = angular.module('timezoneApp.services', ['ngResource']);

services.factory('ErrorHandler', function ($rootScope) {
    var handler = {};
    var genericErrorMessage = "There was a problem during the action. Please try again.";
    var needAuthErrorMessage = "You need to log in.";

    handler.handleError = function (response) {
        $rootScope.hasError = true;
        if (response.status == 401) { //unauthorized
            $rootScope.errorText = needAuthErrorMessage;
        } else {
            var errorMessage = response.data.errorMessage;
            if (errorMessage) {
                $rootScope.errorText = errorMessage;
            } else {
                $rootScope.errorText = genericErrorMessage;
            }
        }
    };

    handler.handleOk = function () {
        $rootScope.hasError = false;
        $rootScope.errorText = null;
    };

    return handler;
});
services.factory('TimezoneDataFactory', function ($resource) {
    return $resource('/api/data/', {}, {
        query: {method: 'GET', isArray: true},
        create: {method: 'POST'}
    })
});
services.factory('TimezoneFactory', function ($resource) {
    return $resource('/api/data/:id', {}, {
        show: {method: 'GET'},
        update: {method: 'PUT', params: {id: '@id'}},
        delete: {method: 'DELETE', params: {id: '@id'}}
    })
});
services.factory('ModeratorTimezoneFactory', function ($resource) {
    return $resource('/api/moderator/data/:id', {}, {
        query: {method: 'GET', isArray: true},
        show: {method: 'GET'},
        update: {method: 'PUT', params: {id: '@id'}},
        delete: {method: 'DELETE', params: {id: '@id'}}
    })
});
services.factory('AdminUserFactory', function ($resource) {
    return $resource('/api/admin/user/:id', {}, {
        query: {method: 'GET', isArray: true},
        show: {method: 'GET'},
        update: {method: 'PUT', params: {id: '@id'}},
        delete: {method: 'DELETE', params: {id: '@id'}}
    })
});
services.factory('CurrentTimeFactory', function ($resource) {
    return $resource('/currentTime/', {}, {
        get: {method: 'GET'}
    })
});
services.factory('RolesFactory', function ($resource) {
    return $resource('/api/admin/roles/', {}, {
        get: {method: 'GET', isArray: true}
    })
});
services.factory('TokenHandler', function ($http) {
    var handler = {};

    handler.hasAuthToken = function() {
        var token = localStorage.getItem(TOKEN_KEY);
        return token != null && token != undefined;
    };

    handler.setJwtToken = function(token) {
        $http.defaults.headers.common['Authorization'] = token;
        localStorage.setItem(TOKEN_KEY, token);
    };

    handler.removeJwtToken = function() {
        $http.defaults.headers.common['Authorization'] = null;
        localStorage.removeItem(TOKEN_KEY);
    };

    return handler;
});
services.factory('AuthService', function($http, $rootScope, TokenHandler, $location) {
    var authService = {};

    authService.checkAuth = function () {
        if (!$rootScope.authenticated) {
            $location.path("/login");
        }
    };

    authService.checkModeratorRole = function () {
        if (!$rootScope.authenticated) {
            $location.path("/login");
        }
    };

    authService.checkAdminRole = function () {
        if (!$rootScope.authenticated) {
            $location.path("/login");
        } else if (!$rootScope.admin) {
            $location.path("/");
        }
    };

    authService.authorize = function (callback) {
        $http.get('user').then(function (response) {
            if (response.data.name) {
                $rootScope.authenticated = true;
                $rootScope.user = response.data.user;
                $rootScope.moderator = response.data.moderator;
                $rootScope.admin = response.data.admin;
            } else {
                logout();
            }
            callback && callback();
        }, function () {
            logout();
            callback && callback();
        });
    };

    authService.logout = function () {
        TokenHandler.removeJwtToken();
        $rootScope.authenticated = false;
        $rootScope.user = false;
        $rootScope.moderator = false;
        $rootScope.admin = false;
    };

    authService.authenticate = function (credentials, callback) {
        $http({
            method: 'POST',
            data: credentials,
            url: '/auth'
        }).then(function (response) {
            var token = response.data.token;
            TokenHandler.setJwtToken(token);
            authService.authorize(callback);
        }, function () {
            authService.logout();
            callback && callback();
        });
    };

    return authService;
});
