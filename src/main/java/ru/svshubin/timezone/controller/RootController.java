package ru.svshubin.timezone.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.svshubin.timezone.model.error.ErrorResponse;
import ru.svshubin.timezone.model.error.ErrorType;
import ru.svshubin.timezone.model.error.ValidationException;
import ru.svshubin.timezone.model.frontend.Metadata;
import ru.svshubin.timezone.model.frontend.Time;
import ru.svshubin.timezone.model.frontend.TimezoneViewOffset;
import ru.svshubin.timezone.model.frontend.TimezoneViewUser;
import ru.svshubin.timezone.model.user.User;
import ru.svshubin.timezone.service.timezone.TimezoneService;
import ru.svshubin.timezone.service.user.UserService;

import java.io.IOException;
import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Sergey Shubin
 */
@RestController
@RequestMapping("/")
public class RootController {
    private UserService userService;
    private TimezoneService timezoneService;

    public RootController(UserService userService, TimezoneService timezoneService) {
        this.userService = Objects.requireNonNull(userService);
        this.timezoneService = Objects.requireNonNull(timezoneService);
    }

    @RequestMapping("/user")
    public ResponseEntity<?> getUser(Principal principal) {
        User user = userService.getUser(principal.getName());

        return ResponseEntity.ok(new TimezoneViewUser(user));
    }

    @RequestMapping("/registerUser")
    public ResponseEntity<?> register(@RequestParam("login") String login,
                                      @RequestParam("password") String password,
                                      @RequestParam("username") String username) throws ValidationException {
        User existingUser = userService.getUser(login);
        if (existingUser != null) {
            throw new ValidationException(new ErrorResponse(ErrorType.HAS_USER_WITH_LOGIN, "Login is already taken, choose another."));
        }

        if (StringUtils.isBlank(login) || StringUtils.isBlank(password)) {
            throw new ValidationException(new ErrorResponse(ErrorType.NO_LOGIN_OR_PASSWORD_PROVIDED, "Specify both the login and the password"));
        }

        userService.registerUser(login, password, username);

        return ResponseEntity.ok(userService.getUser(login));
    }

    @RequestMapping("/metadata")
    public ResponseEntity<?> getMetadata() throws IOException {
        List<TimezoneViewOffset> availableOffsets = timezoneService.getAllOffsets().stream()
                .map(offset -> new TimezoneViewOffset(offset.getId(), offset.getName(), offset.getOffset()))
                .collect(Collectors.toList());
        Metadata metadata = new Metadata(availableOffsets);

        return ResponseEntity.ok(metadata);
    }

    @RequestMapping("/currentTime")
    public ResponseEntity<?> getCurrentTime() throws IOException {
        Time time = new Time(new Date().getTime());

        return ResponseEntity.ok(time);
    }
}
