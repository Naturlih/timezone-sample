package ru.svshubin.timezone.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.svshubin.timezone.model.timezone.TimezoneEntry;
import ru.svshubin.timezone.model.error.ValidationException;
import ru.svshubin.timezone.model.error.ErrorResponse;
import ru.svshubin.timezone.model.error.ErrorType;
import ru.svshubin.timezone.model.frontend.TimezoneViewItem;
import ru.svshubin.timezone.model.user.User;
import ru.svshubin.timezone.service.timezone.TimezoneService;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Sergey Shubin
 */
@RestController
@RequestMapping("/api/moderator/data")
public class ModeratorTimezoneController extends AbstractController {
    private TimezoneService timezoneService;

    public ModeratorTimezoneController(TimezoneService timezoneService) {
        this.timezoneService = Objects.requireNonNull(timezoneService);
    }

    @RequestMapping(method = RequestMethod.GET)
    //TODO
    public ResponseEntity<?> getEntries(@ModelAttribute(MODEL_ATTRIBUTE_USER) User user,
                                        @RequestParam(value = "namePrefix", required = false) String namePrefix) {
        List<TimezoneEntry> entryList = timezoneService.getTimezoneItems();
        if (namePrefix != null) {
            entryList = entryList.stream()
                    .filter(item -> item.getName().startsWith(namePrefix))
                    .collect(Collectors.toList());
        }

        return ResponseEntity.ok(entryList);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getEntry(@ModelAttribute(MODEL_ATTRIBUTE_USER) User user,
                                      @PathVariable("id") int id) {
        TimezoneEntry entry = timezoneService.getTimezoneItem(id);
        if (entry == null) {
            ErrorResponse errorResponse = new ErrorResponse(ErrorType.TIMEZONE_NOT_FOUND, "Cannot find timezone by id " + id);

            return ResponseEntity.badRequest().body(errorResponse);
        }

        return ResponseEntity.ok(entry);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateEntry(@ModelAttribute(MODEL_ATTRIBUTE_USER) User user,
                                         @RequestBody TimezoneViewItem entry,
                                         @PathVariable("id") int id) throws ValidationException {
        timezoneService.update(entry, id);

        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteEntry(@ModelAttribute(MODEL_ATTRIBUTE_USER) User user, @PathVariable("id") int id) {
        timezoneService.delete(id);

        return ResponseEntity.ok().build();
    }
}
