package ru.svshubin.timezone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import ru.svshubin.timezone.model.error.ExceptionWithErrorMessage;
import ru.svshubin.timezone.model.error.ValidationException;
import ru.svshubin.timezone.model.error.ErrorResponse;
import ru.svshubin.timezone.model.error.ErrorType;
import ru.svshubin.timezone.model.user.User;
import ru.svshubin.timezone.service.user.UserService;

import java.security.Principal;
import java.util.Objects;

public abstract class AbstractController {
    protected static final String MODEL_ATTRIBUTE_USER = "user";

    private UserService userService;

    @Autowired
    private void setUserService(UserService userService) {
        this.userService = Objects.requireNonNull(userService);
    }

    @ModelAttribute(MODEL_ATTRIBUTE_USER)
    protected User getUser(Principal principal) {
        if (principal == null) {
            return null;
        }

        return userService.getUser(principal.getName());
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<?> handleBadCredentials() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(ErrorType.BAD_CREDENTIALS, "Bad credentials, please, try again."));
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<?> handleValidationError(ValidationException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getErrorResponse());
    }

    @ExceptionHandler(ExceptionWithErrorMessage.class)
    public ResponseEntity<?> handleExceptionWithInfo(ExceptionWithErrorMessage ex) {
        ex.printStackTrace(); //todo
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getErrorResponse());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleException(Exception ex) {
        ex.printStackTrace(); //todo
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(ErrorType.INTERNAL_SERVER_ERROR, "Internal server error, please, contact administrators."));
    }
}
