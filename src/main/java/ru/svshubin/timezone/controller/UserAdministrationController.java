package ru.svshubin.timezone.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.svshubin.timezone.model.error.ExceptionWithErrorMessage;
import ru.svshubin.timezone.model.error.ValidationException;
import ru.svshubin.timezone.model.error.ErrorResponse;
import ru.svshubin.timezone.model.error.ErrorType;
import ru.svshubin.timezone.model.frontend.ViewUser;
import ru.svshubin.timezone.model.user.Role;
import ru.svshubin.timezone.model.user.User;
import ru.svshubin.timezone.service.user.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Sergey Shubin
 */
@RestController
@RequestMapping("/api/admin")
public class UserAdministrationController extends AbstractController {
    private final UserService userService;

    public UserAdministrationController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public ResponseEntity<?> getUsers() {
        List<User> users = userService.getUsers();
        return ResponseEntity.ok(users);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getUser(@PathVariable("id") int id) {
        User user = userService.getUser(id);
        if (user == null) {
            ErrorResponse errorResponse = new ErrorResponse(ErrorType.USER_NOT_FOUND, "Cannot find user by id " + id);
            return ResponseEntity.badRequest().body(errorResponse);
        }

        return ResponseEntity.ok(user);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateEntry(@ModelAttribute(MODEL_ATTRIBUTE_USER) User admin,
                                         @RequestBody ViewUser user,
                                         @PathVariable("id") int id) throws ValidationException {
        if (user.getId() != id) {
            String errorMessage = "Path user id doesn't match data user id, contact administrators";
            ErrorResponse errorResponse = new ErrorResponse(ErrorType.INTERNAL_SERVER_ERROR, errorMessage);
            throw new ExceptionWithErrorMessage(errorResponse);
        }
        userService.updateUser(user, admin);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteEntry(@PathVariable("id") int id, @ModelAttribute(MODEL_ATTRIBUTE_USER) User admin) throws ValidationException {
        userService.disableUser(id, admin);

        return ResponseEntity.ok().build();
    }

    @RequestMapping("/roles")
    public ResponseEntity<?> getRoles() {
        List<String> roles = new ArrayList<>();
        for (Role role : Role.values()) {
            roles.add(role.getName());
        }

        return ResponseEntity.ok(roles);
    }
}
