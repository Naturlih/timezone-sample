package ru.svshubin.timezone;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.svshubin.timezone.service.timezone.TimezoneDepot;
import ru.svshubin.timezone.service.timezone.TimezoneDepotImpl;
import ru.svshubin.timezone.service.timezone.TimezoneService;
import ru.svshubin.timezone.service.timezone.TimezoneServiceImpl;
import ru.svshubin.timezone.service.user.UserDepot;
import ru.svshubin.timezone.service.user.UserDepotImpl;
import ru.svshubin.timezone.service.user.UserService;
import ru.svshubin.timezone.service.user.UserServiceImpl;

import javax.sql.DataSource;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, "port=9000");
    }

    //DATABASE
    @Bean
    DataSource getDataSource() {
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setServerName("localhost");
        dataSource.setPortNumber(3306);
        dataSource.setUser("root");
        dataSource.setPassword("1234");
        dataSource.setDatabaseName("timezone");

        return dataSource;
    }

    @Bean
    JdbcTemplate getJdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    TimezoneService getTimezoneDataDao(TimezoneDepot timezoneDepot) {
        return new TimezoneServiceImpl(timezoneDepot);
    }

    @Bean
    TimezoneDepot timezoneDepot(JdbcTemplate jdbcTemplate, UserService userService) {
        return new TimezoneDepotImpl(jdbcTemplate, userService);
    }

    @Bean
    UserService getUserDao(UserDepot userDepot) {
        return new UserServiceImpl(userDepot);
    }

    @Bean
    UserDepot userDepot(JdbcTemplate jdbcTemplate) {
        return new UserDepotImpl(jdbcTemplate);
    }
}