package ru.svshubin.timezone.service.user;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import ru.svshubin.timezone.model.user.Role;
import ru.svshubin.timezone.model.user.User;

import java.sql.PreparedStatement;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Sergey Shubin
 */
public class UserDepotImpl implements UserDepot {
    private final JdbcTemplate jdbcTemplate;

    public UserDepotImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = Objects.requireNonNull(jdbcTemplate);
    }

    private static String getSql(String joinWhereClause) {
        String sql = "SELECT u.id, u.login, u.name, u.enabled, group_concat(ur.role) AS roles FROM user AS u" +
                " LEFT JOIN user_role ur ON u.id = ur.user_id AND u.enabled = true %s GROUP BY u.id";
        return String.format(sql, joinWhereClause);
    }

    @Override
    public User getUser(String login) {
        List<User> userList = jdbcTemplate.query(getSql("WHERE login = ?"), new Object[]{login}, getUserRowMapper());
        if (!userList.isEmpty()) {
            return userList.get(0);
        }

        return null;
    }

    @Override
    public String getPasswordForUser(User user) {
        String sql = "SELECT password FROM user WHERE id = ?";
        Object[] args = {user.getId()};
        List<String> password = jdbcTemplate.query(sql, args, (rs, i) -> rs.getString("password"));
        if (password.isEmpty()) {
            return null;
        }

        return password.iterator().next();
    }

    @Override
    public User getUser(int id) {
        String sql = getSql(" WHERE id = ?");
        Object[] args = {id};
        List<User> userList = jdbcTemplate.query(sql, args, getUserRowMapper());
        if (!userList.isEmpty()) {
            return userList.get(0);
        }

        return null;
    }

    @Override
    public List<User> getUsers() {
        return jdbcTemplate.query(getSql(" WHERE u.enabled = true"), getUserRowMapper());
    }

    @Override
    public boolean updateUser(String userName, int id) {
        return jdbcTemplate.update("UPDATE user SET name = ? WHERE id = ?", userName, id) > 0;
    }

    @Override
    public int addUser(String login, String name, String password) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        String sql = "INSERT INTO user(login, name, password, enabled) VALUES (?, ?, ?, ?)";

        jdbcTemplate.update(
                connection -> {
                    PreparedStatement ps = connection.prepareStatement(sql, new String[]{"login", "name", "password", "enabled"});
                    ps.setString(1, login);
                    ps.setString(2, name);
                    ps.setString(3, password);
                    ps.setBoolean(4, true);

                    return ps;
                }, keyHolder);

        return keyHolder.getKey().intValue();
    }

    @Override
    public void deleteRoles(int userId) {
        jdbcTemplate.update("DELETE FROM user_role WHERE user_id = ?", userId);
    }

    @Override
    public void grantRole(int userId, Role role) {
        grantRoles(userId, Collections.singleton(role));
    }

    @Override
    public void grantRoles(int userId, Collection<Role> roles) {
        String sql = "INSERT IGNORE INTO user_role (user_id, role) VALUES (?, ?)";
        List<Object[]> args = new ArrayList<>();
        for (Role role : roles) {
            args.add(new Object[]{userId, role.getName()});
        }

        jdbcTemplate.batchUpdate(sql, args);
    }

    @Override
    public List<String> getRoles(int userId) {
        String sql = "SELECT role FROM user_role WHERE user_id = ?";
        Object[] args = {userId};
        return jdbcTemplate.queryForList(sql, args, String.class);
    }

    @Override
    public void disableUser(int userId) {
        jdbcTemplate.update("UPDATE user SET enabled = FALSE WHERE id = ?", userId);
    }

    private RowMapper<User> getUserRowMapper() {
        return (resultSet, i) -> {
            int userId = resultSet.getInt("id");
            String userKey = resultSet.getString("login");
            String rolesArg = resultSet.getString("roles");
            Collection<Role> roles;
            if (rolesArg != null && !rolesArg.isEmpty() && !rolesArg.equalsIgnoreCase("NULL")) {
                roles = Arrays.stream(rolesArg.split(",")).map(Role::fromName).collect(Collectors.toList());
            } else {
                roles = Collections.emptyList();
            }
            String name = resultSet.getString("name");
            boolean enabled = resultSet.getBoolean("enabled");

            return new User(userId, userKey, enabled, roles, name);
        };
    }

    @Override
    public boolean isEnabled(String login) {
        String sql = "SELECT enabled FROM user WHERE login = ?";
        Object[] args = {login};
        List<Boolean> enabledFlagArg = jdbcTemplate.query(sql, args, ((resultSet, i) -> resultSet.getBoolean("enabled")));
        if (enabledFlagArg.isEmpty()) {
            return false;
        }

        return enabledFlagArg.iterator().next();
    }
}
