package ru.svshubin.timezone.service.user;

import ru.svshubin.timezone.model.error.ExceptionWithErrorMessage;
import ru.svshubin.timezone.model.error.ValidationException;
import ru.svshubin.timezone.model.error.ErrorResponse;
import ru.svshubin.timezone.model.error.ErrorType;
import ru.svshubin.timezone.model.frontend.ViewUser;
import ru.svshubin.timezone.model.user.Role;
import ru.svshubin.timezone.model.user.User;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class UserServiceImpl implements UserService {
    private final UserDepot userDepot;

    public UserServiceImpl(UserDepot userDepot) {
        this.userDepot = Objects.requireNonNull(userDepot);
    }

    @Override
    public synchronized User getUser(String login) {
        return userDepot.getUser(login);
    }

    @Override
    public synchronized User getUser(int id) {
        return userDepot.getUser(id);
    }

    @Override
    public synchronized List<User> getUsers() {
        return userDepot.getUsers();
    }

    @Override
    public synchronized String getPasswordForUser(User user) {
        return userDepot.getPasswordForUser(user);
    }

    @Override
    public synchronized void updateUser(ViewUser user, User admin) throws ValidationException {
        Collection<Role> roles = Arrays.stream(user.getRoles()).map(Role::fromName).collect(Collectors.toList());
        if (user.getId() == admin.getId() && !roles.contains(Role.ROLE_ADMIN)) {
            String errorMessage = "Cannot remove admin role from yourself.";
            ErrorResponse errorResponse = new ErrorResponse(ErrorType.CANNOT_REMOVE_ADMIN_ROLE_FROM_YOURSELF, errorMessage);

            throw new ValidationException(errorResponse);
        }
        boolean hasUser = userDepot.updateUser(user.getName(), user.getId());
        if (hasUser) {
            userDepot.deleteRoles(user.getId());
            userDepot.grantRoles(user.getId(), roles);
        } else {
            String errorMessage = "Cannot find user by id " + user.getId() + ".";
            ErrorResponse errorResponse = new ErrorResponse(ErrorType.USER_NOT_FOUND, errorMessage);

            throw new ExceptionWithErrorMessage(errorResponse);
        }
    }

    @Override
    public synchronized void registerUser(String login, String name, String password) {
        int userId = userDepot.addUser(login, name, password);
        userDepot.grantRole(userId, Role.ROLE_USER);
    }

    @Override
    public synchronized Collection<Role> getRoles(int userId) {
        List<String> roleArgs = userDepot.getRoles(userId);

        return roleArgs.stream().map(Role::fromName).collect(Collectors.toList());
    }

    @Override
    public synchronized void disableUser(int userId, User admin) throws ValidationException {
        if (userId == admin.getId()) {
            throw new ValidationException(new ErrorResponse(ErrorType.CANNOT_REMOVE_YOURSELF, "Cannot delete yourself."));
        }
        userDepot.disableUser(userId);
    }

    @Override
    public boolean isEnabled(String login) {
        return userDepot.isEnabled(login);
    }
}
