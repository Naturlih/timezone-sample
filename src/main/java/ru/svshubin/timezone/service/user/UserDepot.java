package ru.svshubin.timezone.service.user;

import ru.svshubin.timezone.model.user.Role;
import ru.svshubin.timezone.model.user.User;

import java.util.Collection;
import java.util.List;

/**
 * @author Sergey Shubin
 */
public interface UserDepot {
    /**
     * Gets user by login.
     *
     * @param login login of user
     *
     * @return user by login, null, if none can be found
     */
    User getUser(String login);

    /**
     * Gets password of user.
     *
     * @param user user to get password of
     *
     * @return password of user, of null, of no user can be found
     */
    String getPasswordForUser(User user);

    /**
     * Gets user by id.
     *
     * @param id if of user
     *
     * @return user, or null, if no user can be found
     */
    User getUser(int id);

    /**
     * Gets all enabled users.
     *
     * @return list of users.
     */
    List<User> getUsers();

    /**
     * Updates users name by id.
     *
     * @param userName name to set to user
     * @param id       if of user
     *
     * @return true, if found user by id
     */
    boolean updateUser(String userName, int id);

    /**
     * Adds user with specified login, name and password.
     *
     * @param login    login of new user
     * @param name     name of new user
     * @param password password of new user
     *
     * @return id of new user
     */
    int addUser(String login, String name, String password);

    /**
     * Removes all roles from user.
     *
     * @param userId id of user
     */
    void deleteRoles(int userId);

    /**
     * Grants role to user.
     *
     * @param userId id of user
     * @param role   role to grant
     */
    void grantRole(int userId, Role role);

    /**
     * Grants roles to user.
     *
     * @param userId id of user
     * @param roles  roles to grant
     */
    void grantRoles(int userId, Collection<Role> roles);

    /**
     * Get roles of user.
     *
     * @param userId id of user.
     *
     * @return list of roles.
     */
    List<String> getRoles(int userId);

    /**
     * Disables user by id.
     *
     * @param userId id of user
     */
    void disableUser(int userId);

    /**
     * Returns user enabled flag.
     * @param login login of user to check
     * @return true, if user is enabled
     */
    boolean isEnabled(String login);
}
