package ru.svshubin.timezone.service.user;

import ru.svshubin.timezone.model.error.ValidationException;
import ru.svshubin.timezone.model.frontend.ViewUser;
import ru.svshubin.timezone.model.user.Role;
import ru.svshubin.timezone.model.user.User;

import java.util.Collection;
import java.util.List;

/**
 * @author Sergey Shubin
 */
public interface UserService {
    /**
     * Gets user by login.
     *
     * @param login login of user
     *
     * @return user by login
     */
    User getUser(String login);

    /**
     * Gets user by id.
     *
     * @param id if of user
     *
     * @return user
     */
    User getUser(int id);

    /**
     * Gets all enabled users.
     *
     * @return list of users.
     */
    List<User> getUsers();

    /**
     * Gets password of user.
     *
     * @param user user to get password of
     *
     * @return password of user
     */
    String getPasswordForUser(User user);

    /**
     * Updates user.
     * @param user data to update to user
     * @param admin admin, who is performing action
     * @throws ValidationException if tried to remove admin role from himself
     */
    void updateUser(ViewUser user, User admin) throws ValidationException;

    /**
     * Adds user with specified login, name and password and grans ROLE_USER role.
     *
     * @param login    login of new user
     * @param name     name of new user
     * @param password password of new user
     */

    void registerUser(String login, String name, String password);

    /**
     * Get roles of user.
     *
     * @param userId id of user.
     *
     * @return list of roles.
     */
    Collection<Role> getRoles(int userId);

    /**
     * Disables user by id.
     *
     * @param userId id of user
     * @param
     */
    void disableUser(int userId, User admin) throws ValidationException;

    /**
     * Returns user enabled flag.
     * @param login login of user to check
     * @return true, if user is enabled
     */
    boolean isEnabled(String login);
}
