package ru.svshubin.timezone.service.timezone;

import ru.svshubin.timezone.model.error.ExceptionWithErrorMessage;
import ru.svshubin.timezone.model.frontend.TimezoneViewItem;
import ru.svshubin.timezone.model.timezone.TimezoneEntry;
import ru.svshubin.timezone.model.timezone.TimezoneOffset;
import ru.svshubin.timezone.model.user.User;

import java.util.List;

/**
 * @author Sergey Shubin
 */
public interface TimezoneService {
    /**
     * Adds timezone entry in database with specified owner.
     *
     * @param entry entry to add
     * @param user  owner of entry
     */
    void addTimezoneEntry(TimezoneViewItem entry, User user);

    /**
     * Gets timezones of specified user.
     *
     * @param user owner of entry's
     *
     * @return list of entries of user
     */
    List<TimezoneEntry> getTimezoneItems(User user);

    /**
     * Returns all timezone items.
     *
     * @return all timezone items
     */
    List<TimezoneEntry> getTimezoneItems();

    /**
     * Gets timezone item of user by id.
     *
     * @param user owner of timezone
     * @param id   id of timezone
     *
     * @return timezone, if in database, null otherwise
     *
     * @throws ExceptionWithErrorMessage if didn't found timezone by id and owner
     */
    TimezoneEntry getTimezoneItemByPermission(User user, int id) throws ExceptionWithErrorMessage;

    /**
     * Gets timezone item by id.
     *
     * @param id id of timezone
     *
     * @return timezone, if in database, null otherwise
     *
     * @throws ExceptionWithErrorMessage if didn't found timezone by id and owner
     */
    TimezoneEntry getTimezoneItem(int id) throws ExceptionWithErrorMessage;

    /**
     * Updates timezone by id, if owner is specified user.
     *
     * @param item timezone update info
     * @param user owner of timezone
     * @param id   id of timezone
     *
     * @throws ExceptionWithErrorMessage if didn't found timezone by id and owner
     */
    void updateByPermission(TimezoneViewItem item, User user, int id) throws ExceptionWithErrorMessage;

    /**
     * Updates timezone by id.
     *
     * @param item timezone update info
     * @param id   id of timezone
     *
     * @throws ExceptionWithErrorMessage if didn't found timezone by id and owner
     */
    void update(TimezoneViewItem item, int id) throws ExceptionWithErrorMessage;

    /**
     * Gets all available offsets.
     *
     * @return list of offsets.
     */
    List<TimezoneOffset> getAllOffsets();

    /**
     * Deletes timezone by id and specified user owner.
     *
     * @param user owner of timezone
     * @param id   id of timezone
     *
     * @throws ExceptionWithErrorMessage if didn't found timezone by id and owner
     */
    void deleteByPermission(User user, int id) throws ExceptionWithErrorMessage;

    /**
     * Deletes timezone by id.
     *
     * @param id id of timezone
     *
     * @throws ExceptionWithErrorMessage if didn't found timezone by id and owner
     */
    void delete(int id) throws ExceptionWithErrorMessage;
}
