package ru.svshubin.timezone.service.timezone;

import ru.svshubin.timezone.model.error.ExceptionWithErrorMessage;
import ru.svshubin.timezone.model.timezone.TimezoneEntry;
import ru.svshubin.timezone.model.timezone.TimezoneOffset;
import ru.svshubin.timezone.model.error.ValidationException;
import ru.svshubin.timezone.model.error.ErrorResponse;
import ru.svshubin.timezone.model.error.ErrorType;
import ru.svshubin.timezone.model.frontend.TimezoneViewItem;
import ru.svshubin.timezone.model.user.User;

import java.util.List;

/**
 * @author Sergey Shubin
 */
public class TimezoneServiceImpl implements TimezoneService {
    private final TimezoneDepot timezoneDepot;

    public TimezoneServiceImpl(TimezoneDepot timezoneDepot) {
        this.timezoneDepot = timezoneDepot;
    }

    @Override
    public synchronized void addTimezoneEntry(TimezoneViewItem entry, User user) {
        timezoneDepot.addTimezoneEntry(entry, user);
    }

    @Override
    public synchronized List<TimezoneEntry> getTimezoneItems(User user) {
        return timezoneDepot.getTimezoneItems(user);
    }

    @Override
    public synchronized List<TimezoneEntry> getTimezoneItems() {
        return timezoneDepot.getTimezoneItems();
    }

    @Override
    public synchronized TimezoneEntry getTimezoneItemByPermission(User user, int id) {
        TimezoneEntry entry = timezoneDepot.getTimezoneItemByPermission(user, id);
        if (entry == null) {
            String errorMessage = "Cannot find entry by id " + id + " and owner user " + user.getName() + ".";

            throw new ExceptionWithErrorMessage(new ErrorResponse(ErrorType.TIMEZONE_NOT_FOUND, errorMessage));
        }

        return entry;
    }

    @Override
    public synchronized TimezoneEntry getTimezoneItem(int id) {
        TimezoneEntry entry = timezoneDepot.getTimezoneItem(id);
        if (entry == null) {
            String errorMessage = "Cannot find entry by id " + id + ".";

            throw new ExceptionWithErrorMessage(new ErrorResponse(ErrorType.TIMEZONE_NOT_FOUND, errorMessage));
        }

        return entry;
    }

    @Override
    public synchronized void updateByPermission(TimezoneViewItem item, User user, int id) {
        boolean updated = timezoneDepot.updateByPermission(item, user, id);
        if (!updated) {
            String errorMessage = "Cannot find entry by id " + id + " and owner user " + user.getName() + " to update.";

            throw new ExceptionWithErrorMessage(new ErrorResponse(ErrorType.TIMEZONE_NOT_FOUND, errorMessage));
        }
    }

    @Override
    public synchronized void update(TimezoneViewItem item, int id) {
        boolean updated = timezoneDepot.update(item, id);
        if (!updated) {
            String errorMessage = "Cannot find entry by id " + id + " to update.";

            throw new ExceptionWithErrorMessage(new ErrorResponse(ErrorType.TIMEZONE_NOT_FOUND, errorMessage));
        }
    }

    @Override
    public synchronized List<TimezoneOffset> getAllOffsets() {
        return timezoneDepot.getAllOffsets();
    }

    @Override
    public synchronized void deleteByPermission(User user, int id) {
        boolean deleted = timezoneDepot.deleteByPermission(user, id);
        if (!deleted) {
            String errorMessage = "Cannot delete entry by id " + id + " and owner user " + user.getName() + ": entry is not found.";

            throw new ExceptionWithErrorMessage(new ErrorResponse(ErrorType.TIMEZONE_NOT_FOUND, errorMessage));
        }
    }

    @Override
    public synchronized void delete(int id) {
        boolean deleted = timezoneDepot.delete(id);
        if (!deleted) {
            String errorMessage = "Cannot delete entry by id " + id + ": entry is not found.";

            throw new ExceptionWithErrorMessage(new ErrorResponse(ErrorType.TIMEZONE_NOT_FOUND, errorMessage));
        }
    }
}
