package ru.svshubin.timezone.service.timezone;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import ru.svshubin.timezone.model.timezone.TimezoneEntry;
import ru.svshubin.timezone.model.timezone.TimezoneOffset;
import ru.svshubin.timezone.model.frontend.TimezoneViewItem;
import ru.svshubin.timezone.model.user.User;
import ru.svshubin.timezone.service.user.UserService;

import java.util.List;
import java.util.Objects;

/**
 * @author Sergey Shubin
 */
public class TimezoneDepotImpl implements TimezoneDepot {
    private final JdbcTemplate jdbcTemplate;
    private final UserService userService;

    public TimezoneDepotImpl(JdbcTemplate jdbcTemplate, UserService userService) {
        this.jdbcTemplate = Objects.requireNonNull(jdbcTemplate);
        this.userService = Objects.requireNonNull(userService);
    }

    private static String getSqlWithoutUser(String joinWhereClause) {
        return "SELECT tz.id, tz.name, tz.city, off.id, off.name, off.value, u.id, u.login, u.name FROM timezone AS tz" +
                " JOIN offset off ON off.id = tz.offset_id" +
                " JOIN user u ON u.id = tz.user_id " + joinWhereClause;
    }

    private static String getSqlWithUser(String joinWhereClause) {
        return "SELECT tz.id, tz.name, tz.city, off.id, off.name, off.value FROM timezone AS tz" +
                " JOIN offset off ON off.id = tz.offset_id " + joinWhereClause;
    }

    @Override
    public void addTimezoneEntry(TimezoneViewItem entry, User user) {
        jdbcTemplate.update("INSERT INTO timezone (name, city, offset_id, user_id) VALUES (?, ?, ?, ?)",
                entry.getName(), entry.getCity(), entry.getOffsetId(), user.getId());
    }

    @Override
    public List<TimezoneEntry> getTimezoneItems(User user) {
        String sql = getSqlWithUser("WHERE tz.user_id = ?");

        return jdbcTemplate.query(sql, new Object[]{user.getId()}, getTimezoneEntryRowMapper(user));
    }

    @Override
    public List<TimezoneEntry> getTimezoneItems() {
        String sql = getSqlWithoutUser("");

        return jdbcTemplate.query(sql, getTimezoneEntryRowMapper());
    }

    @Override
    public TimezoneEntry getTimezoneItemByPermission(User user, int id) {
        String sql = getSqlWithUser("WHERE tz.user_id = ? AND tz.id = ?");

        List<TimezoneEntry> entries = jdbcTemplate.query(sql, new Object[]{user.getId(), id}, getTimezoneEntryRowMapper(user));
        if (entries.isEmpty()) {
            return null;
        }

        return entries.get(0);
    }

    @Override
    public TimezoneEntry getTimezoneItem(int id) {
        String sql = getSqlWithoutUser("WHERE tz.id = ?");

        List<TimezoneEntry> entries = jdbcTemplate.query(sql, new Object[]{id}, getTimezoneEntryRowMapper());
        if (entries.isEmpty()) {
            return null;
        }

        return entries.get(0);
    }

    @Override
    public boolean updateByPermission(TimezoneViewItem item, User user, int id) {
        return jdbcTemplate.update("UPDATE timezone SET city = ?, offset_id = ?, name = ? WHERE id = ?",
                item.getCity(), item.getOffsetId(), item.getName(), id) > 0;
    }

    @Override
    public boolean update(TimezoneViewItem item, int id) {
        return jdbcTemplate.update("UPDATE timezone SET city = ?, offset_id = ?, name = ? WHERE id = ?",
                item.getCity(), item.getOffsetId(), item.getName(), id) > 0;
    }

    @Override
    public List<TimezoneOffset> getAllOffsets() {
        return jdbcTemplate.query("SELECT * FROM offset",
                (rs, i) -> new TimezoneOffset(rs.getInt("id"), rs.getString("name"), rs.getInt("value")));
    }

    @Override
    public boolean deleteByPermission(User user, int id) {
        return jdbcTemplate.update("DELETE FROM timezone WHERE id = ? AND user_id = ?", id, user.getId()) > 0;
    }

    @Override
    public boolean delete(int id) {
        return jdbcTemplate.update("DELETE FROM timezone WHERE id = ?", id) > 0;
    }

    private RowMapper<TimezoneEntry> getTimezoneEntryRowMapper(User user) {
        return (rs, i) -> {
            TimezoneOffset timezoneOffset = new TimezoneOffset(rs.getInt("off.id"), rs.getString("off.name"), rs.getInt("off.value"));
            return new TimezoneEntry(rs.getInt("tz.id"), rs.getString("tz.name"), user, timezoneOffset, rs.getString("tz.city"));
        };
    }

    private RowMapper<TimezoneEntry> getTimezoneEntryRowMapper() {
        return (rs, i) -> {
            TimezoneOffset timezoneOffset = new TimezoneOffset(rs.getInt("off.id"), rs.getString("off.name"), rs.getInt("off.value"));
            int userId = rs.getInt("u.id");
            User user = userService.getUser(userId);

            return new TimezoneEntry(rs.getInt("tz.id"), rs.getString("tz.name"), user, timezoneOffset, rs.getString("tz.city"));
        };
    }

}
