package ru.svshubin.timezone.model.user;

public enum Role {
    ROLE_USER(0, "ROLE_USER"),
    ROLE_MODERATOR(1, "ROLE_MODERATOR"),
    ROLE_ADMIN(2, "ROLE_ADMIN");

    private final int id;
    private final String name;

    Role(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static Role fromName(String name) {
        for (Role role : values()) {
            if (role.getName().equals(name)) {
                return role;
            }
        }

        return null;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
