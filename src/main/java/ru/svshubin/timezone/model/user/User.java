package ru.svshubin.timezone.model.user;

import java.util.Collection;

public class User {
    private final int id;
    private final String login;
    private final boolean enabled;
    private final Collection<Role> roles;
    private final String name;

    public User(int id, String login, boolean enabled, Collection<Role> roles, String name) {
        this.id = id;
        this.login = login;
        this.enabled = enabled;
        this.roles = roles;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", enabled=" + enabled +
                ", roles=" + roles +
                ", name='" + name + '\'' +
                '}';
    }
}
