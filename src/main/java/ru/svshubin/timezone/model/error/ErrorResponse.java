package ru.svshubin.timezone.model.error;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Sergey Shubin
 */
public class ErrorResponse {
    @JsonIgnore
    private final ErrorType errorType;
    private final String errorMessage;

    public ErrorResponse(ErrorType errorType, String errorMessage) {
        this.errorType = errorType;
        this.errorMessage = errorMessage;
    }

    @JsonGetter("errorType")
    public int getErrorType() {
        return errorType.getId();
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public String toString() {
        return "ErrorResponse{" +
                "errorType=" + errorType +
                ", errorMessage='" + errorMessage + '\'' +
                '}';
    }
}
