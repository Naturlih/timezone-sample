package ru.svshubin.timezone.model.error;

/**
 * @author Sergey Shubin
 */
public class ExceptionWithErrorMessage extends RuntimeException {
    private final ErrorResponse errorResponse;

    public ExceptionWithErrorMessage(ErrorResponse errorResponse) {
        this.errorResponse = errorResponse;
    }

    public ExceptionWithErrorMessage(ErrorResponse errorResponse, Throwable cause) {
        super(cause);
        this.errorResponse = errorResponse;
    }

    public ErrorResponse getErrorResponse() {
        return errorResponse;
    }

    @Override
    public String toString() {
        return "ExceptionWithErrorMessage{" +
                "errorResponse=" + errorResponse +
                '}';
    }
}
