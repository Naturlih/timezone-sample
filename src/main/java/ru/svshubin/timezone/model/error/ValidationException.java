package ru.svshubin.timezone.model.error;

/**
 * @author Sergey Shubin
 */
public class ValidationException extends Exception {
    private final ErrorResponse errorResponse;

    public ValidationException(ErrorResponse errorResponse) {
        this.errorResponse = errorResponse;
    }

    public ErrorResponse getErrorResponse() {
        return errorResponse;
    }

    @Override
    public String toString() {
        return "ValidationException{" +
                "errorResponse=" + errorResponse +
                '}';
    }
}
