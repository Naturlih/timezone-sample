package ru.svshubin.timezone.model.error;

/**
 * @author Sergey Shubin
 */
public enum ErrorType {
    INTERNAL_SERVER_ERROR(1),
    USER_NOT_FOUND(2),
    BAD_CREDENTIALS(3),
    TIMEZONE_NOT_FOUND(4),
    CANNOT_REMOVE_ADMIN_ROLE_FROM_YOURSELF(5),
    CANNOT_REMOVE_YOURSELF(6),
    HAS_USER_WITH_LOGIN(7),
    NO_LOGIN_OR_PASSWORD_PROVIDED(8)
    ;

    private final int id;

    ErrorType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
