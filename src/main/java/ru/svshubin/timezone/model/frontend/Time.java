package ru.svshubin.timezone.model.frontend;

/**
 * @author Sergey Shubin
 */
public class Time {
    private long time;

    public Time(long time) {
        this.time = time;
    }

    public long getTime() {
        return time;
    }

    @Override
    public String toString() {
        return "Time{" +
                "time=" + time +
                '}';
    }
}
