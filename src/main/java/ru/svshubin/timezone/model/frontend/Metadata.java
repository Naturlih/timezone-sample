package ru.svshubin.timezone.model.frontend;

import java.util.List;

/**
 * @author Sergey Shubin
 */
public class Metadata {
    private List<TimezoneViewOffset> availableOffsets;

    public Metadata(List<TimezoneViewOffset> availableOffsets) {
        this.availableOffsets = availableOffsets;
    }

    public List<TimezoneViewOffset> getAvailableOffsets() {
        return availableOffsets;
    }

    @Override
    public String toString() {
        return "Metadata{" +
                "availableOffsets=" + availableOffsets +
                '}';
    }
}
