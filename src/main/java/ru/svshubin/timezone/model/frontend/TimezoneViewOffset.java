package ru.svshubin.timezone.model.frontend;

/**
 * @author Sergey Shubin
 */
public class TimezoneViewOffset {
    private int id;
    private String name;
    private int offset;

    public TimezoneViewOffset() {
    }

    public TimezoneViewOffset(int id, String name, int offset) {
        this.id = id;
        this.name = name;
        this.offset = offset;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getOffset() {
        return offset;
    }

    @Override
    public String toString() {
        return "TimezoneViewOffset{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", offset=" + offset +
                '}';
    }
}
