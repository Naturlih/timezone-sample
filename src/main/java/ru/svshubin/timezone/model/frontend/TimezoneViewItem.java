package ru.svshubin.timezone.model.frontend;

/**
 * @author Sergey Shubin
 */
public class TimezoneViewItem {
    private String name;
    private String city;
    private String offsetId;

    public TimezoneViewItem() {
    }

    public TimezoneViewItem(String name, String city, String offsetId) {
        this.name = name;
        this.city = city;
        this.offsetId = offsetId;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public String getOffsetId() {
        return offsetId;
    }

    @Override
    public String toString() {
        return "TimezoneViewItem{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", offsetId='" + offsetId + '\'' +
                '}';
    }
}
