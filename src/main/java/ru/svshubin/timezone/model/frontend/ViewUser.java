package ru.svshubin.timezone.model.frontend;

import java.util.Arrays;

/**
 * @author Sergey Shubin
 */
public class ViewUser {
    private int id;
    private String name;
    private String[] roles;

    public ViewUser() {
    }

    public ViewUser(int id, String name, String[] roles) {
        this.id = id;
        this.name = name;
        this.roles = roles;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String[] getRoles() {
        return roles;
    }

    @Override
    public String toString() {
        return "ViewUser{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", roles=" + Arrays.toString(roles) +
                '}';
    }
}
