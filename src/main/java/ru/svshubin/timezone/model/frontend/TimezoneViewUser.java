package ru.svshubin.timezone.model.frontend;

import ru.svshubin.timezone.model.user.Role;
import ru.svshubin.timezone.model.user.User;

/**
 * @author Sergey Shubin
 */
public class TimezoneViewUser {
    private final int id;
    private final String login;
    private final String name;
    private final boolean user;
    private final boolean moderator;
    private final boolean admin;

    public TimezoneViewUser(User user) {
        this.id = user.getId();
        this.login = user.getLogin();
        this.name = user.getName();
        this.user = user.getRoles().stream().anyMatch(role -> role == Role.ROLE_USER);
        this.moderator = user.getRoles().stream().anyMatch(role -> role == Role.ROLE_MODERATOR);
        this.admin = user.getRoles().stream().anyMatch(role -> role == Role.ROLE_ADMIN);
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public boolean isModerator() {
        return moderator;
    }

    public boolean isAdmin() {
        return admin;
    }

    public boolean isUser() {
        return user;
    }

    @Override
    public String toString() {
        return "TimezoneViewUser{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", name='" + name + '\'' +
                ", moderator=" + moderator +
                ", admin=" + admin +
                '}';
    }
}
