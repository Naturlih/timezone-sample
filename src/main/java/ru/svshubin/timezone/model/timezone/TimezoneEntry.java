package ru.svshubin.timezone.model.timezone;

import ru.svshubin.timezone.model.user.User;

public class TimezoneEntry {
    private int id;
    private String name;
    private User owner;
    private TimezoneOffset timezoneOffset;
    private String city;

    public TimezoneEntry() {
    }

    public TimezoneEntry(int id, String name, User owner, TimezoneOffset timezoneOffset, String city) {
        this.id = id;
        this.name = name;
        this.owner = owner;
        this.timezoneOffset = timezoneOffset;
        this.city = city;
    }

    //    public TimezoneEntry(int id, User owner, , String city) {
//        this.id = id;
//        this.owner = owner;
//        this.timeZone = TimeZone.getTimeZone(ZoneId.ofOffset("UTC", ZoneOffset.ofHours(offset)));
//        this.city = city;
//    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public User getOwner() {
        return owner;
    }

    public TimezoneOffset getTimezoneOffset() {
        return timezoneOffset;
    }

    public String getCity() {
        return city;
    }

    @Override
    public String toString() {
        return "TimezoneEntry{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", owner=" + owner +
                ", timezoneOffset=" + timezoneOffset +
                ", city='" + city + '\'' +
                '}';
    }
}
