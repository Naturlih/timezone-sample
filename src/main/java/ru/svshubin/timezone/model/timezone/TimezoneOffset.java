package ru.svshubin.timezone.model.timezone;

/**
 * @author Sergey Shubin
 */
public class TimezoneOffset {
    private final int id;
    private final String name;
    private final int offset;

    public TimezoneOffset(int id, String name, int offset) {
        this.id = id;
        this.name = name;
        this.offset = offset;
//        this.timeZone = TimeZone.getTimeZone(ZoneId.ofOffset("UTC", ZoneOffset.ofHours(offset)));
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getOffset() {
        return offset;
    }

    @Override
    public String toString() {
        return "TimezoneOffset{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", offset=" + offset +
                '}';
    }
}
