package ru.svshubin.timezone.security.service;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import ru.svshubin.timezone.model.user.User;
import ru.svshubin.timezone.security.model.JwtUser;
import ru.svshubin.timezone.service.user.UserService;

import java.util.stream.Collectors;

public final class JwtUserFactory {
    private UserService userService;

    public JwtUserFactory(UserService userService) {
        this.userService = userService;
    }

    public JwtUser create(User user) {
        return new JwtUser(
                (long) user.getId(),
                user.getLogin(),
                user.getName(),
                userService.getPasswordForUser(user),
                user.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList())
        );
    }
}
