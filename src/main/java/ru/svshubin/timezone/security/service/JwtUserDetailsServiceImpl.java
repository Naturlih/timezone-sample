package ru.svshubin.timezone.security.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.svshubin.timezone.model.user.User;
import ru.svshubin.timezone.service.user.UserService;

@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {
    private UserService userService;
    private JwtUserFactory jwtUserFactory;

    public JwtUserDetailsServiceImpl(UserService userService, JwtUserFactory jwtUserFactory) {
        this.userService = userService;
        this.jwtUserFactory = jwtUserFactory;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.getUser(username);

        if (user == null) {
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
        } else {
            return jwtUserFactory.create(user);
        }
    }
}
