package ru.svshubin.test.it;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static junit.framework.Assert.assertEquals;

/**
 * @author Sergey Shubin
 */
public class RestApiTest {
    @Test
    public void testAuth() throws IOException {
        JsonObject fakeCredentials = new JsonObject();
        fakeCredentials.addProperty("username", "qweqrqwe");
        fakeCredentials.addProperty("password", "qweqrqwe");
        Response r = post("/auth", fakeCredentials);
        assertEquals(r.getCode(), HttpStatus.BAD_REQUEST.value());
        r = get("/api/data/", "null");
        assertEquals(r.getCode(), HttpStatus.UNAUTHORIZED.value());
        JsonObject correctCredentials = new JsonObject();
        correctCredentials.addProperty("username", "a");
        correctCredentials.addProperty("password", "a");
        r = post("/auth", correctCredentials);
        assertEquals(r.getCode(), HttpStatus.OK.value());
        String token = ((JsonObject) new JsonParser().parse(r.getContent())).get("token").getAsString();
        r = get("/api/data/", token);
        assertEquals(r.getCode(), HttpStatus.OK.value());
        System.out.println("Timezones of user a:");
        System.out.println(r.getContent());
    }

    private Response get(String apiUrl, String authToken) throws IOException {
        String url = "http://localhost:9000" + apiUrl;

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");
        con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
        con.setRequestProperty("Authorization", authToken);

        int responseCode = con.getResponseCode();
        if (responseCode != 200) {
            return new Response(responseCode, "");
        }

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        return new Response(responseCode, response.toString());
    }

    private Response post(String apiUrl, JsonElement element) throws IOException {
        String url = "http://localhost:9000" + apiUrl;

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
        con.setRequestMethod("POST");

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(element.toString());
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        if (responseCode != 200) {
            return new Response(responseCode, "");
        }

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return new Response(responseCode, response.toString());
    }

    private class Response {
        private int code;
        private String content;

        public Response(int code, String content) {
            this.code = code;
            this.content = content;
        }

        public int getCode() {
            return code;
        }

        public String getContent() {
            return content;
        }
    }
}
